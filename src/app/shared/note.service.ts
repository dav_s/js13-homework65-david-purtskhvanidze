import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Note } from './note.model';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class NoteService {
  notesChange = new Subject<Note[]>();
  notesFetching = new Subject<boolean>();
  noteUploading = new Subject<boolean>();

  private notes: Note[] = [];

  constructor(private http: HttpClient) {}

  getNotes() {
    return this.notes.slice();
  }

  fetchNotes() {
    this.notesFetching.next(true);
    this.http.get<{ [id: string]: Note }>('https://rest-b290b-default-rtdb.firebaseio.com/notes.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }

        return Object.keys(result).map(id => {
          const noteData = result[id];
          return new Note(id, noteData.text);
        });
      }))
      .subscribe(notes => {
        this.notes = notes;
        this.notesChange.next(this.notes.slice());
        this.notesFetching.next(false);
      }, () => {
        this.notesFetching.next(false);
      });
  }

  addNote(note: Note) {
    const body = {
      text: note.text,
    };

    this.noteUploading.next(true);

    return this.http.post('https://rest-b290b-default-rtdb.firebaseio.com/notes.json', body).pipe(
      tap(() => {
        this.noteUploading.next(false);
      }, () => {
        this.noteUploading.next(false);
      })
    );
  }

  removeNote(id: string) {
    return this.http.delete('https://rest-b290b-default-rtdb.firebaseio.com/notes/'+ id +'.json');
  }
}
