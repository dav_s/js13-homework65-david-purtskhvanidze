import { Component, OnDestroy, OnInit } from '@angular/core';
import { NoteService } from './shared/note.service';
import { Note } from './shared/note.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {
  notes!: Note[];
  text!: string;
  loading = false;
  notesChangeSubscription!: Subscription;
  notesFetchingSubscription!: Subscription;

  constructor(
    private noteService: NoteService,
  ) {}

  ngOnInit() {
    this.notes = this.noteService.getNotes();
    this.notesChangeSubscription = this.noteService.notesChange.subscribe((notes: Note[]) => {
      this.notes = notes;
    });
    this.notesFetchingSubscription = this.noteService.notesFetching.subscribe((isFetching: boolean) => {
      this.loading = isFetching;
    });
    this.noteService.fetchNotes();
  }

  addNote() {
    const text: string = this.text;
    const id = Math.random().toString();
    const note = new Note(id, text);

    this.noteService.addNote(note).subscribe(() => {
      this.noteService.fetchNotes();
    });
  }

  ngOnDestroy() {
    this.notesChangeSubscription.unsubscribe();
    this.notesFetchingSubscription.unsubscribe();
  }

  removeNote(id: string) {
    this.noteService.removeNote(id).subscribe(() => {
      this.noteService.fetchNotes();
    });
  }
}
